.. sito10min documentation master file, created by
   sphinx-quickstart on Wed Dec 29 16:53:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sito10min's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   00intro.rst
   01strumenti.rst
   02organizzazione.rst
  


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
